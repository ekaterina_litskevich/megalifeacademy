﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Image initialTextImage;
    [SerializeField] private Image infoImage;
    [SerializeField] private Image exitMenuImage;
    [SerializeField] protected Image endingMenuImage;

    [SerializeField] private GameObject fireworks;
    [SerializeField] private Camera mainCamera;

    [SerializeField] private Button consentToGameButton;
    [SerializeField] private Button refusalToGameButton;
    [SerializeField] private Button exitButton;
    [SerializeField] protected Button infoButton;
    [SerializeField] private Button buttonNoInExitMenu;
    [SerializeField] private Button buttonYesInExitMenu;

    protected bool isInfoMenu = false;
    protected bool isInitialText = false;
    private bool isEndingMenu = false;

    protected virtual void OnEnable()
    {
        consentToGameButton.onClick.AddListener(ConsentToGameButton_OnPlayGame);
        refusalToGameButton.onClick.AddListener(ChangeScene);
        exitButton.onClick.AddListener(ExitButton_OnExitGame);
        infoButton.onClick.AddListener(InfoButton_OnInfoText);
        buttonNoInExitMenu.onClick.AddListener(ButtonNoInExitMenu_OnContinue);
        buttonYesInExitMenu.onClick.AddListener(ChangeScene);
    }

    protected virtual void OnDisable()
    {
        consentToGameButton.onClick.RemoveListener(ConsentToGameButton_OnPlayGame);
        refusalToGameButton.onClick.RemoveListener(ChangeScene);
        exitButton.onClick.RemoveListener(ExitButton_OnExitGame);
        infoButton.onClick.RemoveListener(InfoButton_OnInfoText);
        buttonNoInExitMenu.onClick.RemoveListener(ButtonNoInExitMenu_OnContinue);
        buttonYesInExitMenu.onClick.RemoveListener(ChangeScene);
    }

    private void Start()
    {
        isInitialText = true;
    }

    protected virtual void ConsentToGameButton_OnPlayGame()
    {
        isInitialText = false;
        initialTextImage.gameObject.SetActive(false);
        exitButton.gameObject.SetActive(true);
        infoButton.gameObject.SetActive(true);
    }

    private void ChangeScene()
    {
        SceneManager.LoadScene(KeyLybrary.NameMainMenuScene);
    }

    protected virtual void ExitButton_OnExitGame()
    {
        exitMenuImage.gameObject.SetActive(true);
    }

    protected virtual void ButtonNoInExitMenu_OnContinue()
    {
        exitMenuImage.gameObject.SetActive(false);
    }

    protected virtual void InfoButton_OnInfoText()
    {
        if (!isInitialText)
        {
            isInfoMenu = true;
            infoImage.gameObject.SetActive(true);
        }
    }

    protected virtual void EndingText()
    {
        endingMenuImage.gameObject.SetActive(true);
        infoButton.gameObject.SetActive(false);
        exitButton.gameObject.SetActive(false);

        mainCamera.backgroundColor = Color.black;

        if (fireworks != null)
        {
            fireworks.SetActive(true);
        }
        else
        {
            Debug.LogWarning("No fireworks component.");
        }

        isEndingMenu = true;
    }

    protected virtual void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (isInfoMenu)
            {
                infoImage.gameObject.SetActive(false);
                isInfoMenu = false;
            }

            if (isEndingMenu)
            {
                ChangeScene();
            }
        }
    }
}
