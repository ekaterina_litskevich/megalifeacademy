﻿public static class KeyLybrary 
{
    public const string NameMainMenuScene = "MainMenu";
    public const string NameCrossScene = "Cross";
    public const string NameDrawlingScene = "Drawing";
    public const string NameSpeechScene = "Speech";
}
