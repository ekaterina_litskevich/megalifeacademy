﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasCrossScene : UIManager
{
    [SerializeField] private FiguresSpawner figuresSpawner;

    [SerializeField] private Text timerText;
    [SerializeField] private Text amountCrossessText;

    private float minute = 1;
    private float second = 60;

    private bool isStartTimer = false;

    protected override void ConsentToGameButton_OnPlayGame()
    {
        base.ConsentToGameButton_OnPlayGame();

        timerText.gameObject.SetActive(true);
        isStartTimer = true;

        figuresSpawner.SpawnObjects();
    }

    protected override void ExitButton_OnExitGame()
    {
        base.ExitButton_OnExitGame();
        isStartTimer = false;
    }

    protected override void ButtonNoInExitMenu_OnContinue()
    {
        base.ButtonNoInExitMenu_OnContinue();
        isStartTimer = true;
    }

    protected override void InfoButton_OnInfoText()
    {
        base.InfoButton_OnInfoText();

        isStartTimer = false;
    }

    protected override void EndingText()
    {
        base.EndingText();
        figuresSpawner.gameObject.SetActive(false);

        int amountCrosses = figuresSpawner.AmountCrosses;
        int amountCrossedOutCrosses = figuresSpawner.AmountCrossedOutCrosses;

        amountCrossessText.text = string.Format("Правильно зачёркнуто {0}/{1} крестиков", amountCrossedOutCrosses, amountCrosses);
    }

    protected override void Update()
    {
        base.Update();

        if (Input.GetMouseButtonDown(0))
        {
            if (isInfoMenu)
            {
                isStartTimer = true;
            }
        }

        if (isStartTimer)
        {
            bool isCompleted = false;
            if (figuresSpawner.AmountCrossedOutCrosses == figuresSpawner.AmountCrosses)
            {
                timerText.color = Color.green;
                isCompleted = true;
            }
            if (second <= 10 && !isCompleted)
            {
                timerText.color = Color.red;
            }

            if (second <= 0)
            {
                isStartTimer = false;
                timerText.gameObject.SetActive(false);
                infoButton.gameObject.SetActive(false);
                EndingText();
            }

            float timeModifier;
            if (isCompleted)
            {
                timeModifier = 10f;
            }
            else
            {
                timeModifier = 1f;
            }

            if (Convert.ToInt32(minute) > 0)
            {
                timerText.text = string.Format("{0}:00", Convert.ToInt32(minute));
                minute -= Time.deltaTime * timeModifier;
                second -= Time.deltaTime * timeModifier;
            }
            else
            {
                if (Convert.ToInt32(second) >= 10)
                {
                    timerText.text = string.Format("{0}:{1}", Convert.ToInt32(minute), Convert.ToInt32(second));
                }
                else
                {
                    timerText.text = string.Format("{0}:0{1}", Convert.ToInt32(minute), Convert.ToInt32(second));
                }

                second -= Time.deltaTime * timeModifier;
            }
        }        
    }
}
