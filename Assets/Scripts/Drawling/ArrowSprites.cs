﻿using UnityEngine;

[CreateAssetMenu(fileName = "ArrowSprites", menuName = "ArrowSprites")]
public class ArrowSprites : ScriptableObject
{
    [SerializeField] Sprite leftArrowSprite;
    [SerializeField] Sprite rightArrowSprite;
    [SerializeField] Sprite upArrowSprite;
    [SerializeField] Sprite downArrowSprite;

    public Sprite LeftArrowSprite => leftArrowSprite;
    public Sprite RightArrowSprite => rightArrowSprite;
    public Sprite UpArrowSprite => upArrowSprite;
    public Sprite DownArrowSprite => downArrowSprite;
}
