﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
public class Drawing : MonoBehaviour
{
    public event Action OnError;
    public event Action OnEndDrawing;

    private const float LineLength = 1.0f;

    [SerializeField] private Camera mainCamera;
    [SerializeField] private LineRenderer lineRenderer;

    private List<Vector3> points = new List<Vector3>();
    private List<Vector3> correctPoints = new List<Vector3>();

    private Vector2 startPoint;
    private Vector2 endPoint;
    private Vector2 distanceBetweenPoint;

    private int index = 0;
    private int maxAmountLine = 0;

    private bool drawling = false;

    public void StartPoint(List<Vector3> correctPoints)
    {
        this.correctPoints = correctPoints;
        maxAmountLine = correctPoints.Count - 1;
        points.Add(correctPoints[0]);
    }

    public void Restart()
    {
        lineRenderer.positionCount = 0;
        points.Clear();
        index = 0;
    }

    private void DrawlingLine()
    {
        bool lineAdded = false;
        if (drawling)
        {
            Vector3 pointFirst = points[index];
            Vector3 pointTwo = new Vector2();

            if (distanceBetweenPoint.x > LineLength)
            {
                pointTwo = new Vector3(pointFirst.x + LineLength, pointFirst.y);
                lineAdded = true;
            }
            else if (distanceBetweenPoint.y > LineLength)
            {
                pointTwo = new Vector3(pointFirst.x, pointFirst.y + LineLength);
                lineAdded = true;
            }  
            else if (distanceBetweenPoint.y < -LineLength)
            {
                pointTwo = new Vector3(pointFirst.x, pointFirst.y - LineLength);
                lineAdded = true;
            }
            else if (distanceBetweenPoint.x < -LineLength)
            {
                pointTwo = new Vector3(pointFirst.x - LineLength, pointFirst.y);
                lineAdded = true;
            }

            if (lineAdded)
            {
                points.Add(pointTwo);
                lineRenderer.positionCount = points.Count;
                lineRenderer.SetPositions(points.ToArray());

                index++;

                if (points[points.Count - 1] != correctPoints[points.Count - 1])
                {
                    OnError?.Invoke();

                }
                if (index == maxAmountLine)
                {
                    OnEndDrawing?.Invoke();
                }
            }

            drawling = false;
        }
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startPoint = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        }

        if (Input.GetMouseButtonUp(0))
        {
            endPoint = mainCamera.ScreenToWorldPoint(Input.mousePosition);

            distanceBetweenPoint = endPoint - startPoint;

            drawling = true;

            DrawlingLine();
        }

    }
}
