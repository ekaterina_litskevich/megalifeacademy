﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class Arrow : MonoBehaviour
{
    [SerializeField] private Text amountText;
    [SerializeField] private Image arrowImage;

    public Sprite Sprite => arrowImage.sprite;

    public void ChangeText()
    {
        int amount = Int32.Parse(amountText.text);
        amount++;
        amountText.text = string.Format("{0}", amount);
    }

    public void ChangeImage(Sprite sprite)
    {
        arrowImage.sprite = sprite;
    }
}
