﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "IllustrationSourse", menuName = "IllustrationSourse")]
public class IllustrationSourse : ScriptableObject
{
    [SerializeField] List<IllustrationPoints> pointPosition = new List<IllustrationPoints>();

    public IllustrationPoints GetPointsPosition()
    {
        int index = Random.Range(0, pointPosition.Count - 1);
        return pointPosition[index];
    }
}
