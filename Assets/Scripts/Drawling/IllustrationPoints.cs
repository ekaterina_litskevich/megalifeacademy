﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PointsPosition", menuName = "PointsPosition")]
public class IllustrationPoints : ScriptableObject
{
    [SerializeField] List<Vector3> pointPosition = new List<Vector3>();

    public List<Vector3> PointPosition => pointPosition;
}

