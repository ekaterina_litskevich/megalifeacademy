﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CanvasDrawlingScene : UIManager
{
    [SerializeField] private GameObject arrowsPanel;
    [SerializeField] private GameObject grid;
    [SerializeField] private ArrowSprites arrowSprites;
    [SerializeField] private Text endingMenuText;

    [SerializeField] private Arrow arrowPrefab;
    [SerializeField] private IllustrationSourse illustrationSourse;
    [SerializeField] private Drawing drawing;

    private List<Vector3> illustrationPoints = new List<Vector3>();

    private bool isError = false;

    protected override void OnEnable()
    {
        base.OnEnable();

        drawing.OnEndDrawing += EndingText;
        drawing.OnError += WarningText;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        drawing.OnEndDrawing -= EndingText;
        drawing.OnError -= WarningText;
    }

    private void WarningText()
    {
        endingMenuImage.gameObject.SetActive(true);

        endingMenuText.text = string.Format("Попробуй ещё раз!");

        isError = true;
    }

    protected override void EndingText()
    {
        base.EndingText();

        endingMenuText.text = string.Format("Молодец!");
    }

    private void RemoveArrow()
    {
        for (int i = arrowsPanel.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(arrowsPanel.transform.GetChild(i).gameObject);
        }
    }

    protected override void ConsentToGameButton_OnPlayGame()
    {
        base.ConsentToGameButton_OnPlayGame();

        grid.gameObject.SetActive(true);
        arrowsPanel.gameObject.SetActive(true);

        SelectingIllustration();
    }

    private void SelectingIllustration()
    {
        illustrationPoints = illustrationSourse.GetPointsPosition().PointPosition;
        drawing.StartPoint(illustrationPoints);
        PlacementArrows();
    }

    private void PlacementArrows()
    {
        List<Arrow> arrows = new List<Arrow>();

        for (int i = 0; i < illustrationPoints.Count - 1; i++)
        {
            Arrow arrow = Instantiate(arrowPrefab, arrowsPanel.transform);

            if (illustrationPoints[i].x < illustrationPoints[i + 1].x)
            {
                ChangeImageArrow(arrow, arrowSprites.RightArrowSprite);
            }
            else if(illustrationPoints[i].x > illustrationPoints[i + 1].x)
            {
                ChangeImageArrow(arrow, arrowSprites.LeftArrowSprite);
            }
            else if (illustrationPoints[i].y < illustrationPoints[i + 1].y)
            {
                ChangeImageArrow(arrow, arrowSprites.UpArrowSprite);
            }
            else if (illustrationPoints[i].y > illustrationPoints[i + 1].y)
            {
                ChangeImageArrow(arrow, arrowSprites.DownArrowSprite);
            }

            if (i != 0)
            {
                Arrow lastElement = arrows[arrows.Count - 1];

                if (arrow.Sprite == lastElement.Sprite)
                {
                    lastElement.ChangeText();
                    Destroy(arrow.gameObject);
                }
                else
                {
                    arrows.Add(arrow);
                }
            }
            else
            {
                arrows.Add(arrow);
            }
        }
    }

    private void ChangeImageArrow(Arrow arrow, Sprite arrowSprit)
    {
        arrow.ChangeImage(arrowSprit);
    }

    protected override void Update()
    {
        base.Update();

        if (Input.GetMouseButtonDown(0))
        {
            if (isError)
            {
                endingMenuImage.gameObject.SetActive(false);
                RemoveArrow();
                drawing.Restart();
                SelectingIllustration();

                isError = false;
            }
        }
    }
}

